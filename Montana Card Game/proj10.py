############################################
#   Computer Project 10
#
#   Initialize
#   Display(tableau)
#   Validate_move()
#   Move()
#   Shuffle_tableau()
#   Check_win()
#   Main code(Prints the values)
############################################


#DO NOT DELETE THESE LINES
import cards, random
random.seed(100) #random number generator will always generate 
                 #the same random number (needed to replicate tests)


def initialize():
    '''
        Code for Initialize
        This prints the cards into four seperate lists and shuffles them into the starting order
    '''
    
    d_c = cards.Deck()
    n_c = 0
    tableau = [] 
    L1 = []
    d_c.shuffle()
    while n_c != 65:
        n_c += 1  
        if len(L1) == 13:
            tableau.append(L1)
            L1 = []
        else:
            L1.append(d_c.deal())
    return tableau
    pass
    
def display(tableau):
    '''
        This function displays the current state of the game.
        It display four rows of 13 cards with row and column labels.
        Ace is displayed with a blank.
        
        parameters: 
            tableau: data structure representing the tableau 
        
        Returns: None
    '''

    print("{:3s} ".format(' '), end = '')
    for col in range(1,14):
        print("{:3d} ".format(col), end = '')
    print()
        
    for r,row_list in enumerate(tableau):
        print("{:3d}:".format(r+1), end = '')
        for c in row_list:
            if c.rank() == 1:
                print("  {}{}".format(' ',' '), end = '')
            else:
                print("{:>4s}".format(str(c)),end = '')
        print()

def validate_move(tableau,source_row,source_col,dest_row,dest_col):
    '''
        Code for validate_move 
        This code validates the input 
    '''
    s_r = int(source_row)
    s_c = int(source_col)
    d_r = int(dest_row)
    d_c = int(dest_col)
    source_card = tableau[s_r][s_c]
    dest_card = tableau[d_r][d_c]
    if d_c == 0:
        left_card = tableau[d_r][d_c]
    else:
        left_card = tableau[d_r][d_c - 1]
    

    v_move = True
    if (s_r == d_r and s_c == d_c):
        v_move = False
        return v_move
    if int(s_r) in range(0,4) or int(d_c) in range(0,13) or int(s_c) in range(0,13) or int(d_r) in range(0,4):
        v_move = True

    else:
        v_move = False
  
        return v_move        
    if dest_card.rank() != 0:
        v_move = True
  
    else:
        v_move = False
       
        return v_move    
    if (d_c == 0 and source_card.rank() == 2) or (left_card.rank() == source_card.rank() - 1 and dest_card.rank() == 1 and source_card.suit() == left_card.suit()):
        v_move = True
      
    else:
        v_move = False
       
        return v_move
    
    
    
    return v_move
    pass

def move(tableau,source_row,source_col,dest_row,dest_col):
    '''
        Code for move
        This code uses the validate_move function in order to re-format the tableau
    '''
    s_r = int(source_row)
    s_c = int(source_col)
    d_r = int(dest_row)
    d_c = int(dest_col)
    

    x = validate_move(tableau,source_row,source_col,dest_row,dest_col)
    if x == True:
        s_card = tableau[s_r][s_c]
        d_card = tableau[d_r][d_c]
        tableau[s_r][s_c] = d_card
        tableau[d_r][d_c] = s_card
        return True
    else:
        return False
    
    
    
    pass
  
def shuffle_tableau(tableau):
    '''
        Code for shuffle_tableau
        This shuffles the cards
    '''
    row = 0
    new_tableau = []
    list_random = []
    while row != 4:
        col=0
        list_seq_append = True
        list_seq = []
        while col != 13:
            each_card = tableau[row][col]
            length_seq = len(list_seq)
            if col == 0 and each_card.rank() == 1:
                list_seq_append = False
                list_random.append(each_card)
                
            else:
                if len(list_seq) == 0 and list_seq_append == True:
                    list_seq.append(each_card)
                elif each_card.rank() == 1:
                    list_random.append(each_card)
                    list_seq_append = False
                elif list_seq_append == True and list_seq[length_seq-1].rank()+1 == each_card.rank() and list_seq[length_seq-1].suit() == each_card.suit():
                    list_seq.append(each_card)
                else:    
                    list_random.append(each_card)
                    list_seq_append = False
            
            col=col+1
        new_tableau.append(list_seq)
        row=row+1
    random.shuffle(list_random)
    
    ace_list = []
    list_random_without_aces = []
    
    for x in list_random:
        if x.rank() == 1:
            ace_list.append(x)
        else:
            list_random_without_aces.append(x)
          
    list_random_index=0
    new_row = 0
    while new_row != 4:
        length_col = len(new_tableau[new_row])
        new_row_check = True
        while length_col != 13:
            if new_row_check:
                new_tableau[new_row].append(ace_list[new_row])
                length_col = length_col+1
                new_row_check = False
            else:
                new_tableau[new_row].append(list_random_without_aces[list_random_index])
                length_col = length_col+1
                list_random_index = list_random_index+1
        new_row = new_row+1    
    tableau[0] = new_tableau[0]
    tableau[1] = new_tableau[1]
    tableau[2] = new_tableau[2]
    tableau[3] = new_tableau[3]
    
    return tableau
    pass

def check_win(tableau):
    '''
        Code for check_win
        This code is used to check if the game is complete
    '''
    row = 0
    col = 0
    win_flag=False
    while row != 4:
        while col != 11:
            each_card = tableau[row][col]
            next_col = col + 1
            next_card = tableau[row][next_col]
            if(each_card.rank()+1 == next_card.rank() and each_card.suit() == next_card.suit()):
                win_flag = True
            else:
                win_flag = False
                return win_flag
            col=col+1
        row=row+1
        col=0
    return win_flag
        
    pass



def main():
    '''
        Code for main
        This code is used in order to play the game as it complies all the other functions
    '''
    print("Montana Solitaire.")
    tableau = initialize()
    display(tableau)
    step_1 = []
    count = 0
    
    v_m = True
    while 1:
        print("Enter choice: ")
        x = input("(q)uit, (s)huffle, or space-separated: source_row,source_col,dest_row,dest_col: ")
        step_1 = x.split(" ")
        if len(step_1) == 4 and step_1[0].isdigit() == True and step_1[1].isdigit() == True and step_1[2].isdigit() == True and step_1[3].isdigit() == True:
            if int(step_1[0]) in range(1,5) and int(step_1[1]) in range(1,14) and int(step_1[2]) in range(1,5) and int(step_1[3]) in range(1,14):
                source_row = int(step_1[0]) - 1
                source_col = int(step_1[1]) - 1 
                dest_row = int(step_1[2]) - 1 
                dest_col = int(step_1[3]) - 1
                y = len(step_1)
                if y == 4:
                    
                    v_m = validate_move(tableau,source_row,source_col,dest_row,dest_col)
                    if v_m == True:
                        
                        move(tableau,source_row,source_col,dest_row,dest_col)
                        display(tableau)
                        win = check_win(tableau)
                        if win == True:
                            print("You won!")
                            z = input("\nDo you want to play again (y/n)?")
                            if z == "y":
                                print("Montana Solitaire.")
                                tableau = initialize()
                                display(tableau)
                            elif z =="n":
                                print("Thank you for playing.")
                                break   
                    else:
                        print("Error: invalid move. Please try again.")
            else:
                print("Error: row and/or column out of range. Please Try again.")
                
        
        elif x.lower() == "s":
            if count == 2:
                print("No more shuffles remain.")
            else:
                tableau = shuffle_tableau(tableau)
                display(tableau)
                count += 1
        elif x.lower() == "q":
            z = input("Do you want to play again (y/n)?")
            if z == "y":
                print("Montana Solitaire.")
                tableau = initialize()
                display(tableau)
            elif z =="n":
                print("Thank you for playing.")
                break   
            
        
            
        elif len(step_1) < 4:
            print("Error: invalid input. Please try again.")
        
        else: 
            print("Error: invalid input. Please try again.")
    
      
    pass

if __name__ == "__main__":
    main()  