##########################
#   Computer Project #6
#   def less_than(c1, c2)
#   def min_in_list(L)
#   def_cannonical(H)
#   def flush_7(H)
#   def_straight_7(H)
#   def_straight_flush_7(H)
#   def_four_7(H)
#   def_three_7(H)
#   def_two_pair_7(H)
#   def_one_pair_7(H)
#   def_full_house_7(H)
#   def_main()
##########################


import cards

def less_than(c1,c2):
    '''Return 
           True if c1 is smaller in rank, 
           True if ranks are equal and c1 has a 'smaller' suit
           False otherwise'''
    if c1.rank() < c2.rank():
        return True
    elif c1.rank() == c2.rank() and c1.suit() < c2.suit():
        return True
    return False
    
def min_in_list(L):
    '''Return the index of the mininmum card in L'''
    min_card = L[0]  # first card
    min_index = 0
    for i,c in enumerate(L):
        if less_than(c,min_card):  # found a smaller card, c
            min_card = c
            min_index = i
    return min_index
        
def cannonical(H):
    ''' Selection Sort: find smallest and swap with first in H,
        then find second smallest (smallest of rest) and swap with second in H,
        and so on...'''
    for i,c in enumerate(H):
        # get smallest of rest; +i to account for indexing within slice
        min_index = min_in_list(H[i:]) + i 
        H[i], H[min_index] = H[min_index], c  # swap
    return H

def flush_7(H):
    '''Return a list of 5 cards forming a flush,
       if at least 5 of 7 cards form a flush in H, a list of 7 cards, 
       False otherwise.'''
    fl_suit = []
    length = len(H)
    for i in H:
        fl_suit.append(i.suit())
    fl_suit.sort()
    H = cannonical(H)
    i = 0
    while i < (length - 4):
        if fl_suit[i] == fl_suit[i+1] and fl_suit[i] == fl_suit[i+2] and fl_suit[i] == fl_suit[i+3] and fl_suit[i] == fl_suit[i+4]:
            final_flush = []
            counter = 0
            for p in H:
                if p.suit() == fl_suit[i]:
                    if counter < 5:
                        counter += 1
                        final_flush.append(p)
            final_flush = cannonical(final_flush)
            return final_flush
        i += 1
    return False
    pass


def straight_7(H):
    '''Return a list of 5 cards forming a straight,
       if at least 5 of 7 cards form a straight in H, a list of 7 cards, 
       False otherwise.'''
    sorted_hand = cannonical(H)
    i = 0
    final_straight = []
    while i < 3:
        if sorted_hand[i].rank() == (sorted_hand[i+1].rank() - 1) and sorted_hand[i+1].rank() == (sorted_hand[i+2].rank() - 1) and sorted_hand[i+2].rank() == (sorted_hand[i+3].rank() - 1) and sorted_hand[i+3].rank() == (sorted_hand[i+4].rank() - 1): 
            final_straight = (sorted_hand[i:i+5])
            return final_straight
        i += 1
    return False
    pass
        
def straight_flush_7(H):
    '''Return a list of 5 cards forming a straight flush,
       if at least 5 of 7 cards form a straight flush in H, a list of 7 cards, 
       False otherwise.'''
    flush = flush_7(H)
    if flush != False:
        straight = straight_7(flush)
        return straight
            
    return False    
    
    
    pass

def four_7(H):
    '''Return a list of 4 cards with the same rank,
       if 4 of the 7 cards have the same rank in H, a list of 7 cards, 
       False otherwise.'''
    four = cannonical(H)
    i = 0
    final_four = []
    while i < 4:
        if four[i].rank() == four[i+1].rank() and four[i+1].rank() == four[i+2].rank() and four[i+2].rank() == four[i+3].rank():
            final_four = four[i:i+4]
            return final_four
        i += 1
    return False
    pass

def three_7(H):
    '''Return a list of 3 cards with the same rank,
       if 3 of the 7 cards have the same rank in H, a list of 7 cards, 
       False otherwise.
       You may assume that four_7(H) is False.'''
    three = cannonical(H)
    i = 0
    final_three = []
    while i < 5:
        if three[i].rank() == three[i+1].rank() and three[i+1].rank() == three[i+2].rank():
            final_three = three[i:i+3]
            return final_three
        i += 1
    return False
    pass
        
def two_pair_7(H):
    '''Return a list of 4 cards that form 2 pairs,
       if there exist two pairs in H, a list of 7 cards, 
       False otherwise.  
       You may assume that four_7(H) and three_7(H) are both False.'''
    two_pair = cannonical(H)
    i = 0
    pair_counter = 0
    final_two_pair = []
    while i < 6:
        if two_pair[i].rank() == two_pair[i+1].rank():
            final_two_pair.append(two_pair[i])
            final_two_pair.append(two_pair[i+1])
            pair_counter += 1
            i += 1
            if pair_counter == 2:
                return final_two_pair
        i += 1
    return False
    pass

def one_pair_7(H):
    '''Return a list of 2 cards that form a pair,
       if there exists exactly one pair in H, a list of 7 cards, 
       False otherwise.  
       You may assume that four_7(H), three_7(H) and two_pair(H) are False.'''
    one_pair = cannonical(H)
    i = 0
    final_one_pair = []
    length = len(H)
    while i < (length-1):
        if one_pair[i].rank() == one_pair[i+1].rank():
            final_one_pair.append(one_pair[i])
            final_one_pair.append(one_pair[i+1])
            return final_one_pair
        i += 1
    return False
    pass

def full_house_7(H):
    '''Return a list of 5 cards forming a full house,
       if 5 of the 7 cards form a full house in H, a list of 7 cards, 
       False otherwise.
       You may assume that four_7(H) is False.''' 

    three_kind = three_7(H)
    overlap = []
    final_full_house = []
    if three_kind != False:
        i = 0
        while i < 7:
            if three_kind[0].rank() != H[i].rank():
                overlap.append(H[i])
            i += 1
        o_pair = one_pair_7(overlap)
        if o_pair != False:
            final_full_house = three_kind + o_pair
            final_full_house = cannonical(final_full_house)
            return final_full_house
            
    return False
    pass

def main():
    '''
    Main function
    Compiles the functions and prints out the output
    '''
    D = cards.Deck()
    D.shuffle()
    # create community cards
    # create Player 1 hand
    # create Player 2 hand
    counter = 0
    inp = "y"
    while inp == "y":
        i = 0
        hand_1_list = []
        hand_2_list = []
        community_list = []
        hand_1 = hand_1_list + community_list
        hand_2 = hand_2_list + community_list
        while i < 5:
            community_list.append(D.deal()) 
            i += 1
        hand_1_list.append(D.deal())
        hand_1_list.append(D.deal())
        hand_2_list.append(D.deal())
        hand_2_list.append(D.deal())
        hand_1 = hand_1_list + community_list
        hand_2 = hand_2_list + community_list
        print("-"*40)
        print("Let's play poker!\n")
        print("Community cards:",community_list)
        print("Player 1:",hand_1_list)
        print("Player 2:",hand_2_list)
        if flush_7(hand_1) != False and flush_7(hand_2) != False:
            print("TIE with a flush:", flush_7(hand_1))
        elif four_7(hand_1) != False and four_7(hand_2) != False:
            print("TIE with four of a kind:", four_7(hand_1))
        elif four_7(hand_1) != False:
            print("Player 1 wins with four of a kind:", four_7(hand_1))
        elif full_house_7(hand_1) != False:
            print("Player 1 wins with a full house:", full_house_7(hand_1))
        elif two_pair_7(hand_1) != False and two_pair_7(hand_2) != False:
            print("TIE with two pairs:", two_pair_7(hand_1))
        counter += 9
        if counter < 45:
            inp = input("Do you wish to play another hand?(Y or N) ")
        else:
            print("Deck has too few cards so game is done.")
            break
        if inp == "n":
            break
        
    
    



if __name__ == "__main__":
    main()