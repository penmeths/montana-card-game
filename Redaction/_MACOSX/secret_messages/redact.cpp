#include "redact.h"
#include<iostream>
using std::endl;
using std::cout;
#include<string>
using std::string;
#include<vector>
using std::vector;
#include <stdio.h>
#include <ctype.h>

/*This function redacts all the characters of any string provided*/
string redact_all_chars(const string &str){
    int len = str.length();
    string str_1 = str;
    string str_2 = "#";
    for (int i = 0; i < len ; i++){
        str_1.replace(i,1,str_2);
    }
    return str_1;
}
/*This function only redacts alphabetical characters as well as numbers*/
string redact_alphabet_digits(const string &str){
    string str_1 = str;
    string str_2 = "#";
    int len = str.length();
    for (int i = 0; i < len; i++){
        if (isalnum(str[i])){
            str_1.replace(i,1,str_2);
        }
    }
    return str_1;
}
/*This function uses the words given in the vector and compares it to that of the string provided. If the word matches with any part of the string those characters are redacted*/
string redact_words(const string &str, const vector<string> &words_to_redact){
    string str_1 = str;
    int size = words_to_redact.size();
    for (int i = 0; i < size; i++){
        int len = str_1.length();
        for (int x = 0; x < len ; x++){
            if (str_1.substr(x, words_to_redact[i].length()) == words_to_redact[i]){
                string str_2;
                int num = words_to_redact[i].length();
                std::size_t found = str_1.find(words_to_redact[i]);
                if (found!=std::string::npos){
                    for(int j=0; j < num; j++){
                        str_2=str_2+"#";
                    }
                    str_1.replace(found, num, str_2);                
                }
            }    
        }
    }
    return str_1;
}