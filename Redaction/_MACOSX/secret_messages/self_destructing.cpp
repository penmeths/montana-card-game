
#include "self_destructing.h"
#include "redact.h"
#include<vector>
using std::vector;
#include<string>
using std::string;
#include<ios>
using std::istream;
using std::ostream;
#include<iostream>
using std::cout;
using std::endl;

/*Default construcotr*/
SelfDestructingMessage::SelfDestructingMessage(){
    messages = {};
    number_of_allowed_views = 0;
    message_counter= {};

}
/*Initalizing the consturctor*/
SelfDestructingMessage::SelfDestructingMessage(vector<string> messages_1, long number_of_allowed_views_1){
    messages = messages_1;
    number_of_allowed_views = number_of_allowed_views_1;
    int len = messages.size();
    for(int i=0; i< len; i++) {
        message_counter.push_back(number_of_allowed_views_1);
        
    }
    
}
/*Overloading the = operator function*/
SelfDestructingMessage & SelfDestructingMessage::operator=(SelfDestructingMessage & sdm) {
    //cout <<"test" << endl;
    messages = sdm.messages;
    number_of_allowed_views = sdm.number_of_allowed_views;
    message_counter = sdm.message_counter;
    int len = sdm.messages.size();
    //cout << len;

    for(int i=0; i< len; i++) {
        sdm.message_counter[i] = 0;    
    }
    return *this;
}
/*Copy assignment function*/
SelfDestructingMessage::SelfDestructingMessage(
    SelfDestructingMessage & sdm){
    //cout << "heelo" << endl;
    messages = sdm.messages;
    number_of_allowed_views = sdm.number_of_allowed_views;
    message_counter = sdm.message_counter;
    int len = sdm.messages.size();
    //cout << len;

    for(int i=0; i< len; i++) {
        sdm.message_counter[i] = 0;    
    }
}
/*Function returns the size of the vector*/
int SelfDestructingMessage::size(){
    int len = messages.size();
    return len;
}
/*Function redacts the necessary part of the string*/
vector<string> SelfDestructingMessage::get_redacted(){
    int len = messages.size();
    vector<string> result;
    for (int i = 0; i <len ; i++){
        string val = redact_alphabet_digits(messages[i]);  
        result.push_back(val);
        //cout << val << endl;
    }
    return result;
}
/*Overlaoding operator for the [] symbol*/
string SelfDestructingMessage::operator[](int index){
    int len = messages.size();
    if (index >= len || index < 0){
        throw std::out_of_range("too big");
    }
    else if (message_counter[index] == 0){
        throw std::invalid_argument("no more views");
    }
    message_counter[index] = message_counter[index] - 1;
    return messages[index];
}
/*Function for number of views remaining*/
long SelfDestructingMessage::number_of_views_remaining(int message_index){
    long result = message_counter[message_index];
    return result;
    
}
/* Function for an array that appends additional strings to the vector*/
void SelfDestructingMessage::add_array_of_lines(string a[], long a_len){
    for (int i = 0; i < a_len ; i++){
        messages.push_back(a[i]);
        message_counter.push_back(number_of_allowed_views);
    }
}
/*Overlaoding function for <<*/
ostream & operator<<(ostream &oss, SelfDestructingMessage & s){
    
    int len = s.messages.size();
    for (int i = 0 ; i < len; i++){
        
        oss << "0" << s.number_of_views_remaining(i) << ": " << redact_alphabet_digits(s.messages[i])<< endl; 
       
    }
    return oss;
}
/*Overlaoding function for >>*/
istream & operator>>(istream &iss, SelfDestructingMessage & s){
    //int len = s.messages.size();
    //cout << len << endl;
    string temp;
    getline(iss,temp);
    //cout << " New Srtring : " << temp << endl;
    s.messages.push_back(temp);
    s.message_counter.push_back(s.number_of_allowed_views);
    return iss;
}
