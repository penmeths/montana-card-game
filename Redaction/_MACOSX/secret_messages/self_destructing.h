#ifndef SELF_DESTRUCTING_H
#define SELF_DESTRUCTING_H

#include <string>
using std::string;
#include <vector>
using std::vector;
#include <ios>
using std::ostream;
using std::istream;
/*Class SelfDestructingMessage has three constructors and has various function that return the redacted string along with the number of views remaining. If also necessary it adds on additional strings when requried*/
class SelfDestructingMessage {
    private:
        vector<string> messages;
        long number_of_allowed_views;
        vector<long> message_counter; 
    public:
    
    
        SelfDestructingMessage();
        SelfDestructingMessage(vector<string> message_1, long number_of_allowed_views_1);
        
        SelfDestructingMessage(SelfDestructingMessage &);
        SelfDestructingMessage & operator=(SelfDestructingMessage& sdm);
        
        int size();
        vector<string> get_redacted();
        string operator[](int);
        long number_of_views_remaining(int message_index);
        void add_array_of_lines(string a[], long a_len);

    
        friend ostream & operator<<(ostream &oss, SelfDestructingMessage & s);
        friend istream & operator>>(istream &iss, SelfDestructingMessage & s);


       
};
ostream & operator<<(ostream &oss, SelfDestructingMessage & s);
istream & operator>>(istream &iss, SelfDestructingMessage & s);




#endif
